package fr.exo.automate;

public enum States implements Treatment{

    NEUTRAL{
        @Override
        public String processData(String data) {
            return null;
        }

        @Override
        public String processDataFirst(String data) {
            return null;
        }

    },
    TITLE_MAIN {
        @Override
        public String processData(String data) {
            String balise = " \"h1\" : ";
            String[] dataTab = data.split("# ");
            String dataFormated = '"'+ dataTab[1]+'"';
            String dataToReturn = "\t"+balise+dataFormated;

            return  dataToReturn;
        }

        @Override
        public String processDataFirst(String data) {
            return null;
        }



    },
    SUB_TITLE_1_OPENED{
        @Override
        public String processData(String data) {
            String balise = " \"h2\" : ";
            String[] dataTab = data.split("## ");
            String dataFormated = '"'+ dataTab[1]+'"';
            String dataToReturn = "\t"+balise+dataFormated;

            return  dataToReturn;
        }

        @Override
        public String processDataFirst(String data) {
            return null;
        }

    },
    SUB_TITLE_1_CLOSED{
        @Override
        public String processData(String data) {
            return null;
        }

        @Override
        public String processDataFirst(String data) {
            return null;
        }

    },
    SUB_TITLE_2_OPENED{
        @Override
        public String processData(String data) {
            String balise = " \"h3\" : ";
            String[] dataTab = data.split("### ");
            String dataFormated = '"'+ dataTab[1]+'"';
            String dataToReturn = "\t"+balise+dataFormated;

            return  dataToReturn;
        }

        @Override
        public String processDataFirst(String data) {
            return null;
        }

    },
    SUB_TITLE_2_CLOSED{
        @Override
        public String processData(String data) {
            return null;
        }

        @Override
        public String processDataFirst(String data) {
            return null;
        }

    },
    PARA_OPENED{
        @Override
        public String processData(String data) {
            return "\\n"+data;
        }

        @Override
        public String processDataFirst(String data) {
            return "\t"+" \"p\" : "+'"'+ data;
        }

    },
    PARA_CLOSED{
        @Override
        public String processData(String data) {
            return null;
        }

        @Override
        public String processDataFirst(String data) {
            return null;
        }


    },
    CODE_OPENED{
        @Override
        public String processData(String data) {
            if(data.contains("echo ")){
                String[] splittingEcho = data.split("echo ");
                String[] splittingEchoMessage = splittingEcho[1].split("\"");
                return "\\necho "+"\\"+"\""+splittingEchoMessage[1]+"\\"+"\"";
            }else{
                return "\\n"+data;
            }

        }
        @Override
        public String processDataFirst(String data){
            return "\t"+" \"code\" : "+'"'+ data;
        }
    },
    CODE_CLOSED{
        @Override
        public String processData(String data) {
            return "\\n"+data;
        }

        @Override
        public String processDataFirst(String data) {
            return null;
        }

    },
    LIST_OPENED{
        @Override
        public String processData(String data) {
            String[] withoutStar = data.split("\\* ");
            return ", "+'"'+ withoutStar[1]+'"';
        }

        @Override
        public String processDataFirst(String data) {
            String[] withoutStar = data.split("\\* ");
            return "\t"+" \"ul\" : "+'['+'"'+ withoutStar[1]+'"';
        }

    },
    LIST_CLOSED{
        @Override
        public String processData(String data) {
            return ", "+data;
        }

        @Override
        public String processDataFirst(String data) {
            return null;
        }

    },
}
