package fr.exo.automate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;


@SpringBootApplication
public class AutomateApplication {

    public static void main(String[] args) throws IOException {
        SpringApplication.run(AutomateApplication.class, args);


        States currentState = States.NEUTRAL;

        String paragraphe = "";
        String coma = ",";
        File myObj = new File("file.txt");
        try (Scanner myReader = new Scanner(myObj)){

            System.out.println("{");
            while (myReader.hasNextLine()) {

                String data = myReader.nextLine();

                currentState = checkState(currentState, data, paragraphe);

                if(!myReader.hasNextLine()){
                    coma = "";
                }

                ////////////////////////////// CODE BLOC
                //CODE OPENED
                if(currentState.equals(States.CODE_OPENED)){
                    if(data.startsWith("```") && paragraphe != null && !paragraphe.equals("")){
                        currentState = States.CODE_CLOSED;
                    }else if(paragraphe != null && !paragraphe.equals("")) {
                        paragraphe += currentState.processData(data);
                    }else if(paragraphe != null && paragraphe.equals("") && !data.startsWith("```")) {
                        paragraphe = currentState.processDataFirst(data);
                    }
                //CODE CLOSED
                }else if(currentState.equals(States.CODE_CLOSED)){
                    System.out.println(paragraphe+"\""+coma);
                    currentState = States.NEUTRAL;
                    paragraphe = "";

                ////////////////////////////// TITLE MAIN
                }else if(currentState.equals(States.TITLE_MAIN)){
                    System.out.println(currentState.processData(data)+coma);
                    currentState = States.NEUTRAL;
                //////////////////////////////SUB TITLE 1
                }else if(currentState.equals(States.SUB_TITLE_1_OPENED)){
                    System.out.println(currentState.processData(data)+coma);
                    currentState = States.NEUTRAL;
                //////////////////////////////SUB TITLE 2
                }else if(currentState.equals(States.SUB_TITLE_2_OPENED)){
                    System.out.println(currentState.processData(data)+coma);
                    currentState = States.NEUTRAL;
                ////////////////////////////// PARAGRAPHE BLOC
                }else if(currentState.equals(States.PARA_OPENED)){
                    if(paragraphe.equals("")){
                        //process first method
                        paragraphe = currentState.processDataFirst(data);
                    }else if (!paragraphe.isEmpty() && !paragraphe.equals("") && !data.equals("")){
                        paragraphe += currentState.processData(data);
                    }

                    // if end of file
                    if(!myReader.hasNextLine()){
                        System.out.println(paragraphe+'"'+coma);
                        paragraphe = "";
                    // or if blanckline
                    }else if (data.equals("")){
                        System.out.println(paragraphe+'"'+coma);
                        paragraphe = "";
                        currentState = States.NEUTRAL;
                    }
                ////////////////////////////// LIST BLOC
                }else if(currentState.equals(States.LIST_OPENED)){
                    if(paragraphe.equals("")){
                        paragraphe = currentState.processDataFirst(data);
                    }else if(!paragraphe.isEmpty() && !paragraphe.equals("")){
                        paragraphe += currentState.processData(data);

                    }

                    //if end of file
                    if(!myReader.hasNextLine()){
                        System.out.println(paragraphe+']'+coma);
                        paragraphe = "";
                    // or if blanckline
                    }else if (data.equals("")){
                        System.out.println(paragraphe+"]"+coma);
                        paragraphe = "";
                    }

                }

            }
            System.out.println('}');
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        } finally {
            System.out.println("end of program");
        }
    }

    public static States checkState(States currentState, String data, String paragraphe){
        if(currentState.equals(States.NEUTRAL)){
            //BLANK LINE

            //block code
            if(data.startsWith("```")){
                currentState = States.CODE_OPENED;
            }
            //MAIN TITLE
            else if(data.startsWith("# ")){
                currentState = States.TITLE_MAIN;
            }
            //SUB TITLE 1
            else if (data.startsWith("## ")){
                currentState = States.SUB_TITLE_1_OPENED;
            }
            //SUB TITLE 2
            else if (data.startsWith("### ")){
                currentState = States.SUB_TITLE_2_OPENED;
            }
            //LISTE
            else if (data.startsWith("* ") && paragraphe.equals("")){
                currentState = States.LIST_OPENED;
                //PARAGRAPHE
            }else{
                if(!data.equals("") && paragraphe != null && paragraphe.equals("")){
                    currentState = States.PARA_OPENED;
                }
            }
        }
        return currentState;
    }


}

