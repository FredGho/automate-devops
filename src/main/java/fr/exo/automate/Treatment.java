package fr.exo.automate;

public interface Treatment {

    public String processData(String data);


    public String processDataFirst(String data);


}
